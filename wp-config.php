<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_callrox_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')L]+;7(a(#vZf^K(.-2i5~5XHiP3ksn#A`SgDli3}QsLs3fftA}x a}F~j2J0oNs');
define('SECURE_AUTH_KEY',  'fh#Sb{g]B+t.SU5P^L%ng(Y]bo7.@s?doL#2W}_2(Dx(~;HiaA9or6U4!Mo7cBg%');
define('LOGGED_IN_KEY',    '@RFdm4pDueuqNCz%Tf+.8x[t~]2-KEguWfIT_eioa0BOaLg-NXyJ_2R%oQy,G91&');
define('NONCE_KEY',        ';}OGC.^_f4oOx!q^?e8T2J`q1CYBf&23iGG#>s]] 8nU%_,jbFcn~7(E{9dd^+by');
define('AUTH_SALT',        ':2&8 DU1%-?@<rm?~fOP]:W(h1ZKr;zVHS[;ulQ TT=ov;wIDV9E9#}8h>k:d&UH');
define('SECURE_AUTH_SALT', '_4f4pPkADm}Sv^#IxH3F%h21,rq/bYXY%oDj/bA3> a)Dl(Cuc6&v=$  h3Rx3T`');
define('LOGGED_IN_SALT',   'gC)o0*Z;p^>#FB1dG~3C=l=VL<F&R8nWBwwSr(gTkSS@8ff_xrRe>7*YuQ/Yz#3_');
define('NONCE_SALT',       'f{*HY:YBoAIM`c(2%i NX/$z[8d!01U;7SI$)9dpnphEa8H9@Ma-N-U=zI0%MX+3');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cx_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
