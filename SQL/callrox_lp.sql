-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 07-Fev-2019 às 11:43
-- Versão do servidor: 5.6.41
-- versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `callrox_lp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_events`
--

CREATE TABLE `cx_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_failed_logins`
--

CREATE TABLE `cx_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_aiowps_failed_logins`
--

INSERT INTO `cx_aiowps_failed_logins` (`id`, `user_id`, `user_login`, `failed_login_date`, `login_attempt_ip`) VALUES
(1, 1, 'callrox', '2019-02-05 09:42:35', '187.255.129.233');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_global_meta`
--

CREATE TABLE `cx_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_login_activity`
--

CREATE TABLE `cx_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_aiowps_login_activity`
--

INSERT INTO `cx_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'callrox', '2019-02-05 09:42:47', '2019-02-05 09:42:58', '187.255.129.233', '', ''),
(2, 1, 'callrox', '2019-02-05 10:26:19', '0000-00-00 00:00:00', '168.194.160.162', '', ''),
(3, 1, 'callrox', '2019-02-05 15:06:23', '0000-00-00 00:00:00', '187.255.129.233', '', ''),
(4, 1, 'callrox', '2019-02-06 08:38:26', '0000-00-00 00:00:00', '187.255.129.233', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_login_lockdown`
--

CREATE TABLE `cx_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_aiowps_permanent_block`
--

CREATE TABLE `cx_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_commentmeta`
--

CREATE TABLE `cx_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_comments`
--

CREATE TABLE `cx_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_comments`
--

INSERT INTO `cx_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-01-21 10:27:27', '2019-01-21 12:27:27', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_links`
--

CREATE TABLE `cx_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_options`
--

CREATE TABLE `cx_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_options`
--

INSERT INTO `cx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://callrox.com', 'yes'),
(2, 'home', 'https://callrox.com', 'yes'),
(3, 'blogname', 'Callrox', 'yes'),
(4, 'blogdescription', 'Sales is Science', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'agenciahcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";i:4;s:24:\"wordpress-seo/wp-seo.php\";i:5;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:61:\"/home/callrox/public_html/wp-content/themes/callrox/style.css\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'callrox', 'yes'),
(41, 'stylesheet', 'callrox', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '43764', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'cx_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(156, 'rlrsssl_options', 'a:15:{s:12:\"site_has_ssl\";b:1;s:25:\"ssl_success_message_shown\";b:1;s:22:\"htaccess_warning_shown\";b:0;s:19:\"review_notice_shown\";b:0;s:17:\"plugin_db_version\";s:5:\"3.1.3\";s:11:\"ssl_enabled\";b:1;s:9:\"debug_log\";N;s:4:\"hsts\";b:0;s:19:\"javascript_redirect\";b:1;s:11:\"wp_redirect\";b:1;s:26:\"autoreplace_insecure_links\";b:1;s:5:\"debug\";b:0;s:20:\"do_not_edit_htaccess\";b:0;s:31:\"switch_mixed_content_fixer_hook\";b:0;s:17:\"htaccess_redirect\";b:0;}', 'yes'),
(159, '_transient_timeout_wpseo_link_table_inaccessible', '1579609719', 'no'),
(160, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(161, '_transient_timeout_wpseo_meta_table_inaccessible', '1579609719', 'no'),
(162, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:8:{i:1549549648;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1549550038;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1549585648;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1549625638;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1549628857;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1549628919;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1549628992;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1548073785;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(165, 'wysija_post_type_updated', '1548073720', 'yes'),
(166, 'wysija_post_type_created', '1548073720', 'yes'),
(738, '_site_transient_timeout_theme_roots', '1549547813', 'no'),
(739, '_site_transient_theme_roots', 'a:1:{s:7:\"callrox\";s:7:\"/themes\";}', 'no'),
(740, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1549546013;s:7:\"checked\";a:8:{s:19:\"akismet/akismet.php\";s:3:\"4.1\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:7:\"4.3.8.3\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:9:\"hello.php\";s:5:\"1.7.1\";s:28:\"wysija-newsletters/index.php\";s:6:\"2.10.2\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:5:\"3.1.3\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:24:\"wordpress-seo/wp-seo.php\";s:3:\"9.5\";}s:8:\"response\";a:1:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:7:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:7:\"4.3.8.3\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:6:\"2.10.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.10.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:2:\"1x\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/really-simple-ssl\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:6:\"plugin\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:11:\"new_version\";s:5:\"3.1.3\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/really-simple-ssl/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/really-simple-ssl.3.1.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/really-simple-ssl/assets/icon-128x128.png?rev=1782452\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/really-simple-ssl/assets/banner-772x250.jpg?rev=1881345\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"9.5\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wordpress-seo.9.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(168, 'installation_step', '16', 'yes'),
(169, 'wysija', 'YTo4Njp7czo5OiJmcm9tX25hbWUiO3M6NzoiY2FsbHJveCI7czoxMjoicmVwbHl0b19uYW1lIjtzOjc6ImNhbGxyb3giO3M6MTU6ImVtYWlsc19ub3RpZmllZCI7czoxNzoiY2hyaXNAY2FsbHJveC5jb20iO3M6MTA6ImZyb21fZW1haWwiO3M6MTY6ImluZm9AY2FsbHJveC5jb20iO3M6MTM6InJlcGx5dG9fZW1haWwiO3M6MTY6ImluZm9AY2FsbHJveC5jb20iO3M6MTU6ImRlZmF1bHRfbGlzdF9pZCI7aToxO3M6MTc6InRvdGFsX3N1YnNjcmliZXJzIjtzOjE6IjQiO3M6MTY6ImltcG9ydHdwX2xpc3RfaWQiO2k6MjtzOjE4OiJjb25maXJtX2VtYWlsX2xpbmsiO2k6NTtzOjEyOiJ1cGxvYWRmb2xkZXIiO3M6NTI6Ii9ob21lL2NhbGxyb3gvcHVibGljX2h0bWwvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS8iO3M6OToidXBsb2FkdXJsIjtzOjQ2OiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvIjtzOjE2OiJjb25maXJtX2VtYWlsX2lkIjtpOjI7czo5OiJpbnN0YWxsZWQiO2I6MTtzOjIwOiJtYW5hZ2Vfc3Vic2NyaXB0aW9ucyI7aToxO3M6MTQ6Imluc3RhbGxlZF90aW1lIjtpOjE1NDgwNzM3MjA7czoxNzoid3lzaWphX2RiX3ZlcnNpb24iO3M6NjoiMi4xMC4yIjtzOjExOiJka2ltX2RvbWFpbiI7czoxMToiY2FsbHJveC5jb20iO3M6MTY6Ind5c2lqYV93aGF0c19uZXciO3M6NjoiMi4xMC4yIjtzOjI0OiJlbWFpbHNfbm90aWZpZWRfd2hlbl9zdWIiO2k6MTtzOjI2OiJlbWFpbHNfbm90aWZpZWRfd2hlbl91bnN1YiI7YjowO3M6Mjc6ImVtYWlsc19ub3RpZmllZF93aGVuX2JvdW5jZSI7YjowO3M6MzM6ImVtYWlsc19ub3RpZmllZF93aGVuX2RhaWx5c3VtbWFyeSI7YjowO3M6MTk6ImJvdW5jZV9wcm9jZXNzX2F1dG8iO2I6MDtzOjIyOiJtc19ib3VuY2VfcHJvY2Vzc19hdXRvIjtiOjA7czo5OiJzaGFyZWRhdGEiO2I6MDtzOjExOiJka2ltX2FjdGl2ZSI7YjowO3M6OToic210cF9yZXN0IjtiOjA7czoxMjoibXNfc210cF9yZXN0IjtiOjA7czoxNDoiZGVidWdfbG9nX2Nyb24iO2I6MDtzOjIwOiJkZWJ1Z19sb2dfcG9zdF9ub3RpZiI7YjowO3M6MjI6ImRlYnVnX2xvZ19xdWVyeV9lcnJvcnMiO2I6MDtzOjIzOiJkZWJ1Z19sb2dfcXVldWVfcHJvY2VzcyI7YjowO3M6MTY6ImRlYnVnX2xvZ19tYW51YWwiO2I6MDtzOjE1OiJjb21wYW55X2FkZHJlc3MiO3M6MDoiIjtzOjE2OiJ1bnN1YnNjcmliZV9wYWdlIjtzOjE6IjUiO3M6MTc6ImNvbmZpcm1fZGJsZW9wdGluIjtzOjE6IjAiO3M6MTc6ImNvbmZpcm1hdGlvbl9wYWdlIjtzOjE6IjUiO3M6OToic210cF9ob3N0IjtzOjA6IiI7czoxMDoic210cF9sb2dpbiI7czowOiIiO3M6MTM6InNtdHBfcGFzc3dvcmQiO3M6MDoiIjtzOjk6InNtdHBfcG9ydCI7czoyOiIyNSI7czoxMToic210cF9zZWN1cmUiO3M6MToiMCI7czoxMDoidGVzdF9tYWlscyI7czozNToiYWdlbmNpYWhjZGVzZW52b2x2aW1lbnRvc0BnbWFpbC5jb20iO3M6MTI6ImJvdW5jZV9lbWFpbCI7czowOiIiO3M6MjY6Im1hbmFnZV9zdWJzY3JpcHRpb25zX2xpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE4OiJzdWJzY3JpcHRpb25zX3BhZ2UiO3M6MToiNSI7czoxMToiaHRtbF9zb3VyY2UiO3M6MToiMCI7czo4OiJpbmR1c3RyeSI7czo1OiJvdXRybyI7czoxNjoiYXJjaGl2ZV9saW5rbmFtZSI7czoxNjoiW3d5c2lqYV9hcmNoaXZlXSI7czoyNjoic3Vic2NyaWJlcnNfY291bnRfbGlua25hbWUiO3M6MjY6Ilt3eXNpamFfc3Vic2NyaWJlcnNfY291bnRdIjtzOjEzOiJhcmNoaXZlX2xpc3RzIjthOjE6e2k6MTtiOjA7fXM6Mzg6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLW5ld3NsZXR0ZXJzIjtiOjA7czozMToicm9sZXNjYXAtLS1lZGl0b3ItLS1uZXdzbGV0dGVycyI7YjowO3M6MzE6InJvbGVzY2FwLS0tYXV0aG9yLS0tbmV3c2xldHRlcnMiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tbmV3c2xldHRlcnMiO2I6MDtzOjM1OiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS1uZXdzbGV0dGVycyI7YjowO3M6Mzg6InJvbGVzY2FwLS0td3BzZW9fbWFuYWdlci0tLW5ld3NsZXR0ZXJzIjtiOjA7czozNzoicm9sZXNjYXAtLS13cHNlb19lZGl0b3ItLS1uZXdzbGV0dGVycyI7YjowO3M6Mzg6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLXN1YnNjcmliZXJzIjtiOjA7czozMToicm9sZXNjYXAtLS1lZGl0b3ItLS1zdWJzY3JpYmVycyI7YjowO3M6MzE6InJvbGVzY2FwLS0tYXV0aG9yLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjM1OiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS1zdWJzY3JpYmVycyI7YjowO3M6Mzg6InJvbGVzY2FwLS0td3BzZW9fbWFuYWdlci0tLXN1YnNjcmliZXJzIjtiOjA7czozNzoicm9sZXNjYXAtLS13cHNlb19lZGl0b3ItLS1zdWJzY3JpYmVycyI7YjowO3M6MzM6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLWNvbmZpZyI7YjowO3M6MjY6InJvbGVzY2FwLS0tZWRpdG9yLS0tY29uZmlnIjtiOjA7czoyNjoicm9sZXNjYXAtLS1hdXRob3ItLS1jb25maWciO2I6MDtzOjMxOiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tY29uZmlnIjtiOjA7czozMDoicm9sZXNjYXAtLS1zdWJzY3JpYmVyLS0tY29uZmlnIjtiOjA7czozMzoicm9sZXNjYXAtLS13cHNlb19tYW5hZ2VyLS0tY29uZmlnIjtiOjA7czozMjoicm9sZXNjYXAtLS13cHNlb19lZGl0b3ItLS1jb25maWciO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWFkbWluaXN0cmF0b3ItLS10aGVtZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWVkaXRvci0tLXRoZW1lX3RhYiI7YjowO3M6Mjk6InJvbGVzY2FwLS0tYXV0aG9yLS0tdGhlbWVfdGFiIjtiOjA7czozNDoicm9sZXNjYXAtLS1jb250cmlidXRvci0tLXRoZW1lX3RhYiI7YjowO3M6MzM6InJvbGVzY2FwLS0tc3Vic2NyaWJlci0tLXRoZW1lX3RhYiI7YjowO3M6MzY6InJvbGVzY2FwLS0td3BzZW9fbWFuYWdlci0tLXRoZW1lX3RhYiI7YjowO3M6MzU6InJvbGVzY2FwLS0td3BzZW9fZWRpdG9yLS0tdGhlbWVfdGFiIjtiOjA7czozNjoicm9sZXNjYXAtLS1hZG1pbmlzdHJhdG9yLS0tc3R5bGVfdGFiIjtiOjA7czoyOToicm9sZXNjYXAtLS1lZGl0b3ItLS1zdHlsZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWF1dGhvci0tLXN0eWxlX3RhYiI7YjowO3M6MzQ6InJvbGVzY2FwLS0tY29udHJpYnV0b3ItLS1zdHlsZV90YWIiO2I6MDtzOjMzOiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS1zdHlsZV90YWIiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLXdwc2VvX21hbmFnZXItLS1zdHlsZV90YWIiO2I6MDtzOjM1OiJyb2xlc2NhcC0tLXdwc2VvX2VkaXRvci0tLXN0eWxlX3RhYiI7YjowO30=', 'yes'),
(163, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(409, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1549546012;s:15:\"version_checked\";s:5:\"5.0.3\";s:12:\"translations\";a:0:{}}', 'no'),
(296, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1548321723;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(656, '_site_transient_timeout_browser_05a35df476881e356975dc07ce8a30ab', '1549974380', 'no'),
(657, '_site_transient_browser_05a35df476881e356975dc07ce8a30ab', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(639, 'aiowpsec_db_version', '1.9', 'yes'),
(640, 'aio_wp_security_configs', 'a:94:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:1:\"1\";s:25:\"aiowps_prevent_hotlinking\";s:1:\"1\";s:28:\"aiowps_enable_login_lockdown\";s:1:\"1\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";i:3;s:24:\"aiowps_retry_time_period\";i:0;s:26:\"aiowps_lockout_time_length\";i:60;s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:33:\"suporte@hcdesenvolvimentos.com.br\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"y8hmfj3och5qgs30ktew\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:1:\"1\";s:34:\"aiowps_enable_custom_login_captcha\";s:1:\"1\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"26yvfx4p9i9rxwcfgf4j\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:1:\"1\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:35:\"agenciahcdesenvolvimentos@gmail.com\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:1:\"1\";s:31:\"aiowps_enable_pingback_firewall\";s:1:\"1\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:1:\"1\";s:34:\"aiowps_block_debug_log_file_access\";s:1:\"1\";s:26:\"aiowps_disable_index_views\";s:1:\"1\";s:30:\"aiowps_disable_trace_and_track\";s:1:\"1\";s:28:\"aiowps_forbid_proxy_comments\";s:1:\"1\";s:29:\"aiowps_deny_bad_query_strings\";s:1:\"1\";s:34:\"aiowps_advanced_char_string_filter\";s:1:\"1\";s:25:\"aiowps_enable_5g_firewall\";s:1:\"1\";s:25:\"aiowps_enable_6g_firewall\";s:1:\"1\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:1:\"1\";s:28:\"aiowps_enable_login_honeypot\";s:1:\"1\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:1:\"1\";s:29:\"aiowps_enable_comment_captcha\";s:1:\"1\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:1:\"1\";s:25:\"aiowps_fcd_scan_frequency\";i:4;s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:33:\"suporte@hcdesenvolvimentos.com.br\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:1:\"1\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";s:25:\"aiowps_recaptcha_site_key\";s:0:\"\";s:27:\"aiowps_recaptcha_secret_key\";s:0:\"\";s:24:\"aiowps_default_recaptcha\";s:0:\"\";s:28:\"aiowps_block_fake_googlebots\";s:1:\"1\";s:22:\"aiowps_login_page_slug\";s:6:\"painel\";s:35:\"aiowps_enable_lost_password_captcha\";s:1:\"1\";s:25:\"aiowps_last_fcd_scan_time\";s:19:\"2019-02-05 10:36:04\";}', 'yes'),
(613, '_site_transient_timeout_browser_0ac1f9240df96b3586c220faef490724', '1549971173', 'no'),
(614, '_site_transient_browser_0ac1f9240df96b3586c220faef490724', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(136, 'can_compress_scripts', '1', 'no'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(172, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1549549611;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1548160123;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1549623386;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1549891075;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1550492923;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}}', 'yes'),
(634, 'rewrite_rules', 'a:91:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(204, 'category_children', 'a:0:{}', 'yes'),
(152, 'wpseo', 'a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"9.5\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1548073719;s:18:\"recalibration_beta\";b:0;}', 'yes'),
(153, 'wpseo_titles', 'a:67:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:40:\"%%name%%, Autor em %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:66:\"Você pesquisou por %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:44:\"Página não encontrada %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:54:\"O post %%POSTLINK%% apareceu primeiro em %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:33:\"Erro 404: Página não encontrada\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:13:\"Arquivos para\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:7:\"Início\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:19:\"Você pesquisou por\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:18:\"title-tax-category\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;}', 'yes'),
(154, 'wpseo_social', 'a:20:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(155, 'wpseo_flush_rewrite', '1', 'yes'),
(170, 'wysija_reinstall', '0', 'no'),
(173, 'rsssl_activation_timestamp', '1548073724', 'yes'),
(180, 'current_theme', 'CallRox', 'yes'),
(181, 'theme_mods_callrox', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(182, 'theme_switched', '', 'yes'),
(183, 'redux_version_upgraded_from', '3.6.15', 'yes');
INSERT INTO `cx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(184, 'configuracao', 'a:31:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_titulo\";s:62:\"Sales is <strong>science.</strong>  Not a talent of rockstars.\";s:13:\"opt_descricao\";s:98:\"We exist to discover the pattern of your top sales performers and make your business grow. Faster.\";s:23:\"opt_comoFunciona_titulo\";s:42:\"<strong>Work smarter. Not harder.</strong>\";s:25:\"opt_comoFunciona_conteudo\";s:237:\"<p class=\"fora\">We combine speech-to-text technology, artificial intelligence and coaching features to discover the patterns of your top performers and help you coach your team in a more <strong>productive and effective way.</strong></p>\";s:26:\"opt_comoFunciona_conteudo2\";s:197:\"<p class=\"fora\">Have a complete <strong>sales coaching</strong> software with the features you need to identify improvement points and manage your team\'s ongoing development easily and smartly.</p>\";s:20:\"opt_vantagens_titulo\";s:17:\"In data we trust.\";s:29:\"opt_vantagens_topico_1_titulo\";s:24:\"Increase Conversion Rate\";s:26:\"opt_vantagens_topico_1_img\";a:9:{s:3:\"url\";s:62:\"https://callrox.com/wp-content/uploads/2019/01/conversao-2.png\";s:2:\"id\";s:2:\"10\";s:6:\"height\";s:3:\"164\";s:5:\"width\";s:3:\"190\";s:9:\"thumbnail\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/conversao-2-150x150.png\";s:5:\"title\";s:11:\"conversao-2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:29:\"opt_vantagens_topico_2_titulo\";s:19:\"Shorter Sales Cycle\";s:26:\"opt_vantagens_topico_2_img\";a:9:{s:3:\"url\";s:63:\"https://callrox.com/wp-content/uploads/2019/01/fechamento-2.png\";s:2:\"id\";s:2:\"11\";s:6:\"height\";s:3:\"164\";s:5:\"width\";s:3:\"190\";s:9:\"thumbnail\";s:71:\"https://callrox.com/wp-content/uploads/2019/01/fechamento-2-150x150.png\";s:5:\"title\";s:12:\"fechamento-2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:29:\"opt_vantagens_topico_3_titulo\";s:21:\"Average Ticket Growth\";s:26:\"opt_vantagens_topico_3_img\";a:9:{s:3:\"url\";s:59:\"https://callrox.com/wp-content/uploads/2019/01/ticket-1.png\";s:2:\"id\";s:2:\"12\";s:6:\"height\";s:3:\"164\";s:5:\"width\";s:3:\"190\";s:9:\"thumbnail\";s:67:\"https://callrox.com/wp-content/uploads/2019/01/ticket-1-150x150.png\";s:5:\"title\";s:8:\"ticket-1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:29:\"opt_vantagens_topico_4_titulo\";s:19:\"Faster Ramp up Time\";s:26:\"opt_vantagens_topico_4_img\";a:9:{s:3:\"url\";s:59:\"https://callrox.com/wp-content/uploads/2019/01/rampup-2.png\";s:2:\"id\";s:2:\"13\";s:6:\"height\";s:3:\"164\";s:5:\"width\";s:3:\"190\";s:9:\"thumbnail\";s:67:\"https://callrox.com/wp-content/uploads/2019/01/rampup-2-150x150.png\";s:5:\"title\";s:8:\"rampup-2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_logos1\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logos1_link\";s:1:\"#\";s:10:\"opt_logos2\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logos2_link\";s:1:\"#\";s:10:\"opt_logos3\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logos3_link\";s:1:\"#\";s:10:\"opt_logos4\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logos4_link\";s:1:\"#\";s:10:\"opt_logos5\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logos5_link\";s:1:\"#\";s:17:\"opt_rodape_titulo\";s:44:\"How much money are you leaving on the table?\";s:20:\"opt_rodape_subtitulo\";s:55:\"Eliminate the guesswork and start using words that sell\";s:20:\"opt_rodape_copyright\";s:56:\"Copyright  <i class=\"far fa-copyright\"></i> 2019 Callrox\";s:26:\"opt_formulario_botao_texto\";s:10:\"Let`s Talk\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(185, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:5:{s:10:\"opt_logos1\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_logos2\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_logos3\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_logos4\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:10:\"opt_logos5\";a:9:{s:3:\"url\";s:70:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png\";s:2:\"id\";s:1:\"9\";s:6:\"height\";s:3:\"243\";s:5:\"width\";s:4:\"1176\";s:9:\"thumbnail\";s:78:\"https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta-150x150.png\";s:5:\"title\";s:19:\"Marca-Callrox-Preta\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:9:\"last_save\";i:1549449520;}', 'yes'),
(189, 'wysija_queries', '', 'no'),
(190, 'wysija_queries_errors', '', 'no'),
(191, 'wysija_msg', '', 'no'),
(194, 'new_admin_email', 'agenciahcdesenvolvimentos@gmail.com', 'yes'),
(209, 'wysija_last_php_cron_call', '1549546581', 'yes'),
(207, 'wysija_check_pn', '1549546011.9242', 'yes'),
(208, 'wysija_last_scheduled_check', '1549546011', 'yes'),
(410, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1549546013;s:7:\"checked\";a:1:{s:7:\"callrox\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_postmeta`
--

CREATE TABLE `cx_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_postmeta`
--

INSERT INTO `cx_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(6, 7, '_wp_page_template', 'template-parts/inicial.php'),
(5, 7, '_edit_lock', '1548073815:1'),
(7, 7, '_edit_last', '1'),
(8, 7, '_yoast_wpseo_content_score', '30'),
(9, 9, '_wp_attached_file', '2019/01/Marca-Callrox-Preta.png'),
(10, 9, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1176;s:6:\"height\";i:243;s:4:\"file\";s:31:\"2019/01/Marca-Callrox-Preta.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"Marca-Callrox-Preta-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"Marca-Callrox-Preta-300x62.png\";s:5:\"width\";i:300;s:6:\"height\";i:62;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"Marca-Callrox-Preta-768x159.png\";s:5:\"width\";i:768;s:6:\"height\";i:159;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"Marca-Callrox-Preta-1024x212.png\";s:5:\"width\";i:1024;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:31:\"Marca-Callrox-Preta-600x124.png\";s:5:\"width\";i:600;s:6:\"height\";i:124;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 10, '_wp_attached_file', '2019/01/conversao-2.png'),
(12, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:164;s:4:\"file\";s:23:\"2019/01/conversao-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"conversao-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 11, '_wp_attached_file', '2019/01/fechamento-2.png'),
(14, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:164;s:4:\"file\";s:24:\"2019/01/fechamento-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"fechamento-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(15, 12, '_wp_attached_file', '2019/01/ticket-1.png'),
(16, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:164;s:4:\"file\";s:20:\"2019/01/ticket-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"ticket-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 13, '_wp_attached_file', '2019/01/rampup-2.png'),
(18, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:164;s:4:\"file\";s:20:\"2019/01/rampup-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"rampup-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 14, '_form', '<label> Seu nome (obrigatório)\n    [text* your-name] </label>\n\n<label> Seu e-mail (obrigatório)\n    [email* your-email] </label>\n\n<label> Assunto\n    [text your-subject] </label>\n\n<label> Sua mensagem\n    [textarea your-message] </label>\n\n[submit \"Enviar\"]'),
(20, 14, '_mail', 'a:8:{s:7:\"subject\";s:24:\"CallRox \"[your-subject]\"\";s:6:\"sender\";s:31:\"CallRox <wordpress@callrox.com>\";s:4:\"body\";s:180:\"De: [your-name] <[your-email]>\nAssunto: [your-subject]\n\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\";s:9:\"recipient\";s:35:\"agenciahcdesenvolvimentos@gmail.com\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(21, 14, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"CallRox \"[your-subject]\"\";s:6:\"sender\";s:31:\"CallRox <wordpress@callrox.com>\";s:4:\"body\";s:124:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:45:\"Reply-To: agenciahcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(22, 14, '_messages', 'a:8:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:63:\"Um ou mais campos possuem um erro. Verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";}'),
(23, 14, '_additional_settings', NULL),
(24, 14, '_locale', 'pt_BR'),
(25, 15, '_form', '[text* nome placeholder \"Name\"][text* phone placeholder \"Phone\"][email* email placeholder \"Email\"]<div class=\"btnEnviarinput\">[submit \"Let\'s Talk\"]</div>'),
(26, 15, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:22:\"Formulário de contato\";s:6:\"sender\";s:29:\"CallRox <contato@callrox.com>\";s:9:\"recipient\";s:17:\"chris@callrox.com\";s:4:\"body\";s:142:\"De: [nome] \n\nEmail: <[email]>\n\nTelefone: [phone]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Callrox (https://callrox.com.br)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(27, 15, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"CallRox \"[your-subject]\"\";s:6:\"sender\";s:31:\"CallRox <wordpress@callrox.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:124:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\";s:18:\"additional_headers\";s:45:\"Reply-To: agenciahcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(28, 15, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Thank you for your message.\";s:12:\"mail_sent_ng\";s:70:\"There was an error while trying to send your message. Try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:70:\"There was an error while trying to send your message. Try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:24:\"The field is very short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is earlier than the oldest allowed.\";s:13:\"date_too_late\";s:48:\"The date is later than the highest date allowed.\";s:13:\"upload_failed\";s:51:\"An unknown error occurred while uploading the file.\";s:24:\"upload_file_type_invalid\";s:53:\"You do not have permission to send this type of file.\";s:21:\"upload_file_too_large\";s:22:\"The file is too large.\";s:23:\"upload_failed_php_error\";s:39:\"There was an error submitting the file.\";s:14:\"invalid_number\";s:25:\"Number format is invalid.\";s:16:\"number_too_small\";s:44:\"The number is less than the minimum allowed.\";s:16:\"number_too_large\";s:47:\"The number is greater than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:37:\"The entered email address is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:28:\"The phone number is invalid.\";}'),
(29, 15, '_additional_settings', ''),
(30, 15, '_locale', 'pt_BR');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_posts`
--

CREATE TABLE `cx_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_posts`
--

INSERT INTO `cx_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-01-21 10:27:27', '2019-01-21 12:27:27', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-01-21 10:27:27', '2019-01-21 12:27:27', '', 0, 'https://callrox.com/?p=1', 0, 'post', '', 1),
(2, 1, '2019-01-21 10:27:27', '2019-01-21 12:27:27', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"https://callrox.com/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2019-01-21 10:27:27', '2019-01-21 12:27:27', '', 0, 'https://callrox.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-01-21 10:27:27', '2019-01-21 12:27:27', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: https://callrox.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-01-21 10:27:27', '2019-01-21 12:27:27', '', 0, 'https://callrox.com/?page_id=3', 0, 'page', '', 0),
(17, 1, '2019-02-05 09:32:53', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-02-05 09:32:53', '0000-00-00 00:00:00', '', 0, 'https://callrox.com/?p=17', 0, 'post', '', 0),
(5, 1, '2019-01-21 10:28:40', '2019-01-21 12:28:40', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2019-01-21 10:28:40', '2019-01-21 12:28:40', '', 0, 'https://callrox.com/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(7, 1, '2019-01-21 10:30:14', '2019-01-21 12:30:14', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-01-21 10:30:14', '2019-01-21 12:30:14', '', 0, 'https://callrox.com/?page_id=7', 0, 'page', '', 0),
(8, 1, '2019-01-21 10:30:14', '2019-01-21 12:30:14', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-01-21 10:30:14', '2019-01-21 12:30:14', '', 7, 'https://callrox.com/2019/01/21/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-01-21 10:33:37', '2019-01-21 12:33:37', '', 'Marca-Callrox-Preta', '', 'inherit', 'open', 'closed', '', 'marca-callrox-preta', '', '', '2019-01-21 10:33:37', '2019-01-21 12:33:37', '', 0, 'https://callrox.com/wp-content/uploads/2019/01/Marca-Callrox-Preta.png', 0, 'attachment', 'image/png', 0),
(10, 1, '2019-01-21 10:41:41', '2019-01-21 12:41:41', '', 'conversao-2', '', 'inherit', 'open', 'closed', '', 'conversao-2', '', '', '2019-01-21 10:41:41', '2019-01-21 12:41:41', '', 0, 'https://callrox.com/wp-content/uploads/2019/01/conversao-2.png', 0, 'attachment', 'image/png', 0),
(11, 1, '2019-01-21 10:41:57', '2019-01-21 12:41:57', '', 'fechamento-2', '', 'inherit', 'open', 'closed', '', 'fechamento-2', '', '', '2019-01-21 10:41:57', '2019-01-21 12:41:57', '', 0, 'https://callrox.com/wp-content/uploads/2019/01/fechamento-2.png', 0, 'attachment', 'image/png', 0),
(12, 1, '2019-01-21 10:42:13', '2019-01-21 12:42:13', '', 'ticket-1', '', 'inherit', 'open', 'closed', '', 'ticket-1', '', '', '2019-01-21 10:42:13', '2019-01-21 12:42:13', '', 0, 'https://callrox.com/wp-content/uploads/2019/01/ticket-1.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2019-01-21 10:42:21', '2019-01-21 12:42:21', '', 'rampup-2', '', 'inherit', 'open', 'closed', '', 'rampup-2', '', '', '2019-01-21 10:42:21', '2019-01-21 12:42:21', '', 0, 'https://callrox.com/wp-content/uploads/2019/01/rampup-2.png', 0, 'attachment', 'image/png', 0),
(14, 1, '2019-01-24 09:22:03', '2019-01-24 11:22:03', '<label> Seu nome (obrigatório)\n    [text* your-name] </label>\n\n<label> Seu e-mail (obrigatório)\n    [email* your-email] </label>\n\n<label> Assunto\n    [text your-subject] </label>\n\n<label> Sua mensagem\n    [textarea your-message] </label>\n\n[submit \"Enviar\"]\nCallRox \"[your-subject]\"\nCallRox <wordpress@callrox.com>\nDe: [your-name] <[your-email]>\nAssunto: [your-subject]\n\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\nagenciahcdesenvolvimentos@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nCallRox \"[your-subject]\"\nCallRox <wordpress@callrox.com>\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\n[your-email]\nReply-To: agenciahcdesenvolvimentos@gmail.com\n\n0\n0\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.', 'Formulário de contato 1', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato-1', '', '', '2019-01-24 09:22:03', '2019-01-24 11:22:03', '', 0, 'https://callrox.com/?post_type=wpcf7_contact_form&p=14', 0, 'wpcf7_contact_form', '', 0),
(15, 1, '2019-01-24 09:27:01', '2019-01-24 11:27:01', '[text* nome placeholder \"Name\"][text* phone placeholder \"Phone\"][email* email placeholder \"Email\"]<div class=\"btnEnviarinput\">[submit \"Let\'s Talk\"]</div>\n1\nFormulário de contato\nCallRox <contato@callrox.com>\nchris@callrox.com\nDe: [nome] \r\n\r\nEmail: <[email]>\r\n\r\nTelefone: [phone]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Callrox (https://callrox.com.br)\n\n\n\n\n\nCallRox \"[your-subject]\"\nCallRox <wordpress@callrox.com>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em CallRox (https://callrox.com)\nReply-To: agenciahcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message.\nThere was an error while trying to send your message. Try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error while trying to send your message. Try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is very short.\nThe date format is incorrect.\nThe date is earlier than the oldest allowed.\nThe date is later than the highest date allowed.\nAn unknown error occurred while uploading the file.\nYou do not have permission to send this type of file.\nThe file is too large.\nThere was an error submitting the file.\nNumber format is invalid.\nThe number is less than the minimum allowed.\nThe number is greater than the maximum allowed.\nThe answer to the quiz is incorrect.\nO código digitado está incorreto.\nThe entered email address is invalid.\nThe URL is invalid.\nThe phone number is invalid.', 'Formulário de Contato', '', 'publish', 'closed', 'closed', '', 'sem-titulo', '', '', '2019-01-31 10:00:12', '2019-01-31 12:00:12', '', 0, 'https://callrox.com/?post_type=wpcf7_contact_form&#038;p=15', 0, 'wpcf7_contact_form', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_termmeta`
--

CREATE TABLE `cx_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_terms`
--

CREATE TABLE `cx_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_terms`
--

INSERT INTO `cx_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_term_relationships`
--

CREATE TABLE `cx_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_term_relationships`
--

INSERT INTO `cx_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_term_taxonomy`
--

CREATE TABLE `cx_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_term_taxonomy`
--

INSERT INTO `cx_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_usermeta`
--

CREATE TABLE `cx_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_usermeta`
--

INSERT INTO `cx_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'callrox'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cx_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cx_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(27, 1, 'session_tokens', 'a:3:{s:64:\"3ce31b19525fdf00fa2d957e97b6112bfb5c640c24134ad576ce2c064497a3c3\";a:4:{s:10:\"expiration\";i:1549542379;s:2:\"ip\";s:15:\"168.194.160.162\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1549369579;}s:64:\"d1d4d1cd9b577f5aeb48aa87a530d3067cee0611e5292c13bce33756b6f8fc5e\";a:4:{s:10:\"expiration\";i:1549559183;s:2:\"ip\";s:15:\"187.255.129.233\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1549386383;}s:64:\"f78b619406e679d25f5328ff07e4d1d35c2b3fcd40758a14d1bb2694d84c9d27\";a:4:{s:10:\"expiration\";i:1549622306;s:2:\"ip\";s:15:\"187.255.129.233\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1549449506;}}'),
(17, 1, 'cx_dashboard_quick_press_last_post_id', '17'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"187.255.129.0\";}'),
(19, 1, 'cx_yoast_notifications', 'a:3:{i:0;a:2:{s:7:\"message\";s:1143:\"Percebemos que você já está usando o Yoast SEO há algum tempo; esperamos que esteja gostando! Nós adoraríamos se você pudesse <a href=\"https://yoa.st/rate-yoast-seo?php_version=7.0.33&platform=wordpress&platform_version=5.0.3&software=free&software_version=9.5&role=administrator&days_active=16\">nos classificar com 5 estrelas no WordPress.org</a>!\n\nSe estiver enfrentando problemas, <a href=\"https://yoa.st/bugreport?php_version=7.0.33&platform=wordpress&platform_version=5.0.3&software=free&software_version=9.5&role=administrator&days_active=16\">envie um relatório de erro</a> e faremos tudo o que pudermos para ajudar você.\n\nBy the way, did you know we also have a <a href=\'https://yoa.st/premium-notification?php_version=7.0.33&platform=wordpress&platform_version=5.0.3&software=free&software_version=9.5&role=administrator&days_active=16\'>Premium plugin</a>? It offers advanced features, like a redirect manager and support for multiple keyphrases. It also comes with 24/7 personal support.\n\n<a class=\"button\" href=\"https://callrox.com/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Não mostre mais essa notificação</a>\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.8000000000000000444089209850062616169452667236328125;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:183:\"Não deixe escapar seus erros de rastreamento: <a href=\"https://callrox.com/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">conecte-se com o Google Search Console aqui</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:2;a:2:{s:7:\"message\";s:438:\"We&#039;d love for you to try our new and improved Yoast SEO analysis! Use the toggle on the <a href=\"#top#features\" onclick=\"jQuery(\'#features-tab\').click()\">Features tab</a> in your Yoast SEO settings. <a href=\"https://yoa.st/recalibration-beta-notice?php_version=7.0.33&platform=wordpress&platform_version=5.0.3&software=free&software_version=9.5&role=administrator&days_active=16\" target=\"_blank\">Read more about the new analysis</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:37:\"wpseo-recalibration-meta-notification\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(22, 1, 'cx_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1548160365;}'),
(23, 1, 'cx_user-settings', 'libraryContent=browse&editor=html'),
(24, 1, 'cx_user-settings-time', '1548074710'),
(20, 1, '_yoast_wpseo_profile_updated', '1548073787'),
(21, 1, 'wysija_pref', 'YTowOnt9'),
(26, 1, 'last_login_time', '2019-02-06 08:38:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_users`
--

CREATE TABLE `cx_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_users`
--

INSERT INTO `cx_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'callrox', '$P$B89Y3VdryFYZ1OPn2TbuhdNMfKoWdb/', 'callrox', 'agenciahcdesenvolvimentos@gmail.com', '', '2019-01-21 12:27:27', '', 0, 'callrox');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_campaign`
--

CREATE TABLE `cx_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_campaign`
--

INSERT INTO `cx_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_campaign_list`
--

CREATE TABLE `cx_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_custom_field`
--

CREATE TABLE `cx_wysija_custom_field` (
  `id` mediumint(9) NOT NULL,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_custom_field`
--

INSERT INTO `cx_wysija_custom_field` (`id`, `name`, `type`, `required`, `settings`) VALUES
(1, 'Phone', 'input', 1, 'a:2:{s:8:\"required\";s:1:\"1\";s:8:\"validate\";s:5:\"phone\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_email`
--

CREATE TABLE `cx_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_email`
--

INSERT INTO `cx_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Problemas de visualização? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"https://callrox.com/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"https://callrox.com/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"https://callrox.com/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"https://callrox.com/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"https://callrox.com/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"https://callrox.com/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"https://callrox.com/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"https://callrox.com/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 4:</strong> e o rodapé?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"https://callrox.com/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1548073720, 1548073720, NULL, 'info@callrox.com', 'callrox', 'info@callrox.com', 'callrox', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEwMjoiaHR0cHM6Ly9jYWxscm94LmNvbS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjk6InRodW1iX3VybCI7czoxMTA6Imh0dHBzOi8vY2FsbHJveC5jb20vd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLTE1MHgxNTAucG5nIjt9fX0=', 'YTo0OntzOjc6InZlcnNpb24iO3M6NjoiMi4xMC4yIjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMDI6Imh0dHBzOi8vY2FsbHJveC5jb20vd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvaGVhZGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NzI7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImhlYWRlciI7fXM6NDoiYm9keSI7YTo5OntzOjc6ImJsb2NrLTEiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTYwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNVG84TDNOMGNtOXVaejRnWldrc0lHTnNhWEYxWlNCdVpYTjBaU0IwWlhoMGJ5RThMMmd5UGp4d1BsQmhjbUVnWldScGRHRnlMQ0J6YVcxd2JHVnpiV1Z1ZEdVZ1kyeHBjWFZsSUc1bGMzUmxJR0pzYjJOdklHUmxJSFJsZUhSdkxqd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjY0OiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTMiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6ODA6IlBHZ3lQanh6ZEhKdmJtYytVR0Z6YzI4Z01qbzhMM04wY205dVp6NGdZbkpwYm5GMVpTQmpiMjBnWlhOMFlTQnBiV0ZuWlcwOEwyZ3lQZz09Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNCI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czo3NjoiUEhBK1VHOXphV05wYjI1bElHOGdjMlYxSUcxdmRYTmxJR0ZqYVcxaElHUmhJR2x0WVdkbGJTRERvQ0JsYzNGMVpYSmtZUzQ4TDNBKyI7fXM6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTAyOiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6NToid2lkdGgiO2k6MjgxO3M6NjoiaGVpZ2h0IjtpOjE5MDtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NDtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNSI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjU7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjY0OiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTYiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MzAwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNem84TDNOMGNtOXVaejRnYzI5c2RHVWdZMjl1ZEdYRHVtUnZJR0Z4ZFdrOEwyZ3lQanh3UGtGeWNtRnpkR1VnWlNCemIyeDBaU0E4YzNSeWIyNW5QblJsZUhSdkxDQndiM04wY3l3Z1pHbDJhWE52Y21Wekxqd3ZjM1J5YjI1blBpQldaV3BoSUc1dklHeGhaRzhnWkdseVpXbDBieUU4TDNBK1BIQStWbTlqdzZvZ2NHOWtaU0JoZE1PcElHTnlhV0Z5SUR4emRISnZibWMrWTI5dGNHRnlkR2xzYUdGdFpXNTBiM01nYzI5amFXRnBjend2YzNSeWIyNW5QaUJqYjIxdklHVnpkR1Z6T2p3dmNEND0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjc4OiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi9mYWNlYm9vay5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MTthOjc6e3M6MzoidXJsIjtzOjMyOiJodHRwOi8vd3d3LnR3aXR0ZXIuY29tL21haWxfcG9ldCI7czozOiJhbHQiO3M6NzoiVHdpdHRlciI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo3NzoiaHR0cHM6Ly9jYWxscm94LmNvbS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvdHdpdHRlci5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MjthOjc6e3M6MzoidXJsIjtzOjMzOiJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS8rTWFpbHBvZXQiO3M6MzoiYWx0IjtzOjY6Ikdvb2dsZSI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo3NjoiaHR0cHM6Ly9jYWxscm94LmNvbS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjY0OiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTkiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTg4OiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdORG84TDNOMGNtOXVaejRnWlNCdklISnZaR0Z3dzZrL1BDOW9NajQ4Y0Q1TmIyUnBabWx4ZFdVZ2J5QmpiMjUwWmNPNlpHOGdaRzhnY205a1lYRERxU0J1WVNBOGMzUnliMjVuUG5ERG9XZHBibUVnWkdVZ2IzRERwOE8xWlhNOEwzTjBjbTl1Wno0Z1pHOGdUV0ZwYkZCdlpYUXVQQzl3UGc9PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjk7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO319czo2OiJmb290ZXIiO2E6NTp7czo0OiJ0ZXh0IjtOO3M6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTAyOiJodHRwczovL2NhbGxyb3guY29tL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2Zvb3Rlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjQ2O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJmb290ZXIiO319', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em CallRox', 'Olá!\r\n\r\nLegal! Você se inscreveu como assinante em nosso site.\r\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \r\n\r\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\r\n\r\nObrigado,\r\n\r\nA equipe!\r\n', 1548073720, 1548073720, NULL, 'info@callrox.com', 'callrox', 'info@callrox.com', 'callrox', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_email_user_stat`
--

CREATE TABLE `cx_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_email_user_url`
--

CREATE TABLE `cx_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_form`
--

CREATE TABLE `cx_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_form`
--

INSERT INTO `cx_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjczOiJWZWphIHN1YSBjYWl4YSBkZSBlbnRyYWRhIG91IHBhc3RhIGRlIHNwYW0gcGFyYSBjb25maXJtYXIgc3VhIGFzc2luYXR1cmEuIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE3OiJsaXN0c19zZWxlY3RlZF9ieSI7czo1OiJhZG1pbiI7fXM6NDoiYm9keSI7YToyOntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6NToiRW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czo4OiJyZXF1aXJlZCI7YjoxO319aToxO2E6NDp7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6ODoiQXNzaW5hciEiO319fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0),
(2, 'Formulário de Contato', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIyIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo2NDoiVEhBTksgWU9VIQoKV2Ugd2lsbCByZXNwb25kIHRvIHlvdXIgbWVzc2FnZSBhcyBzb29uIGFzIHBvc3NpYmxlLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6NDp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjM6e3M6NToibGFiZWwiO3M6NDoiTm9tZSI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjEiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6OToiZmlyc3RuYW1lIjtzOjQ6Im5hbWUiO3M6NDoiTm9tZSI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjY6InBhcmFtcyI7YTo0OntzOjU6ImxhYmVsIjtzOjU6IlBob25lIjtzOjEyOiJsYWJlbF93aXRoaW4iO3M6MToiMSI7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjg6InZhbGlkYXRlIjtzOjU6InBob25lIjt9czo4OiJwb3NpdGlvbiI7aToyO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo0OiJjZl8xIjtzOjQ6Im5hbWUiO3M6NToiUGhvbmUiO31zOjc6ImJsb2NrLTMiO2E6NTp7czo2OiJwYXJhbXMiO2E6Mzp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjEiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6NToiZW1haWwiO3M6NDoibmFtZSI7czo1OiJFbWFpbCI7fXM6NzoiYmxvY2stNCI7YTo1OntzOjY6InBhcmFtcyI7YToxOntzOjU6ImxhYmVsIjtzOjEwOiJMZXQncyBUYWxrIjt9czo4OiJwb3NpdGlvbiI7aTo0O3M6NDoidHlwZSI7czo2OiJzdWJtaXQiO3M6NToiZmllbGQiO3M6Njoic3VibWl0IjtzOjQ6Im5hbWUiO3M6NjoiRW52aWFyIjt9fXM6NzoiZm9ybV9pZCI7aToyO30=', NULL, 2),
(3, 'Formulário de contato rodapé', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIzIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo2NDoiVEhBTksgWU9VIQoKV2Ugd2lsbCByZXNwb25kIHRvIHlvdXIgbWVzc2FnZSBhcyBzb29uIGFzIHBvc3NpYmxlLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6NDp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjM6e3M6NToibGFiZWwiO3M6NDoiTm9tZSI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjEiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6OToiZmlyc3RuYW1lIjtzOjQ6Im5hbWUiO3M6NDoiTm9tZSI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjY6InBhcmFtcyI7YTo0OntzOjU6ImxhYmVsIjtzOjU6IlBob25lIjtzOjEyOiJsYWJlbF93aXRoaW4iO3M6MToiMSI7czo4OiJyZXF1aXJlZCI7czoxOiIxIjtzOjg6InZhbGlkYXRlIjtzOjU6InBob25lIjt9czo4OiJwb3NpdGlvbiI7aToyO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo0OiJjZl8xIjtzOjQ6Im5hbWUiO3M6NToiUGhvbmUiO31zOjc6ImJsb2NrLTMiO2E6NTp7czo2OiJwYXJhbXMiO2E6Mzp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czoxMjoibGFiZWxfd2l0aGluIjtzOjE6IjEiO3M6ODoicmVxdWlyZWQiO3M6MToiMSI7fXM6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6NToiaW5wdXQiO3M6NToiZmllbGQiO3M6NToiZW1haWwiO3M6NDoibmFtZSI7czo1OiJFbWFpbCI7fXM6NzoiYmxvY2stNCI7YTo1OntzOjY6InBhcmFtcyI7YToxOntzOjU6ImxhYmVsIjtzOjEwOiJMZXQncyBUYWxrIjt9czo4OiJwb3NpdGlvbiI7aTo0O3M6NDoidHlwZSI7czo2OiJzdWJtaXQiO3M6NToiZmllbGQiO3M6Njoic3VibWl0IjtzOjQ6Im5hbWUiO3M6NjoiRW52aWFyIjt9fXM6NzoiZm9ybV9pZCI7aTozO30=', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_list`
--

CREATE TABLE `cx_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_list`
--

INSERT INTO `cx_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Minha primeira lista', 'minha-primeira-lista', 'A lista criada automaticamente na instalação do MailPoet.', 0, 0, 1, 1, 1548073720, 0),
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1548073720, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_queue`
--

CREATE TABLE `cx_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_subscriber_ips`
--

CREATE TABLE `cx_wysija_subscriber_ips` (
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_subscriber_ips`
--

INSERT INTO `cx_wysija_subscriber_ips` (`ip`, `created_at`) VALUES
('187.255.129.233', '2019-01-21 12:53:32'),
('187.255.129.233', '2019-01-21 12:56:52'),
('138.204.24.254', '2019-01-22 12:39:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_url`
--

CREATE TABLE `cx_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_url_mail`
--

CREATE TABLE `cx_wysija_url_mail` (
  `email_id` int(11) NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_user`
--

CREATE TABLE `cx_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT '',
  `cf_1` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_user`
--

INSERT INTO `cx_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`, `cf_1`) VALUES
(1, 1, 'agenciahcdesenvolvimentos@gmail.com', '', '', '', '0', NULL, NULL, NULL, '4f57a17ad8977d7c351383dc7da1a434', 1548073721, 1, 'gmail.com', NULL),
(2, 0, 'teste@teste.com', 'teste', '', '187.255.129.233', '0', NULL, NULL, NULL, '79e4e526aab15eddfc0542a59794be4f', 1548075212, 0, 'teste.com', '123'),
(3, 0, 'teste@tesxte.com', 'teste', '', '187.255.129.233', '0', NULL, NULL, NULL, 'f35b8d139d7cc2e535fffa1b001158fa', 1548075412, 0, 'tesxte.com', '123'),
(4, 0, 'chris@audiotext.com.br', 'Christiano Cornehl', '', '138.204.24.254', '0', NULL, NULL, NULL, '8c97c0b4dee16816416b0c77aaa43188', 1548160766, 0, 'audiotext.com.br', '41996025123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_user_field`
--

CREATE TABLE `cx_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_user_field`
--

INSERT INTO `cx_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_user_history`
--

CREATE TABLE `cx_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_wysija_user_list`
--

CREATE TABLE `cx_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cx_wysija_user_list`
--

INSERT INTO `cx_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1548073720, 0),
(2, 1, 1548073720, 0),
(1, 2, 0, 0),
(1, 3, 0, 0),
(1, 4, 1548160766, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_yoast_seo_links`
--

CREATE TABLE `cx_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cx_yoast_seo_meta`
--

CREATE TABLE `cx_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cx_yoast_seo_meta`
--

INSERT INTO `cx_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(6, 0, 0),
(7, 0, 0),
(4, 0, 0),
(16, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cx_aiowps_events`
--
ALTER TABLE `cx_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_aiowps_failed_logins`
--
ALTER TABLE `cx_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_aiowps_global_meta`
--
ALTER TABLE `cx_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `cx_aiowps_login_activity`
--
ALTER TABLE `cx_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_aiowps_login_lockdown`
--
ALTER TABLE `cx_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_aiowps_permanent_block`
--
ALTER TABLE `cx_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_commentmeta`
--
ALTER TABLE `cx_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cx_comments`
--
ALTER TABLE `cx_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `cx_links`
--
ALTER TABLE `cx_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `cx_options`
--
ALTER TABLE `cx_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `cx_postmeta`
--
ALTER TABLE `cx_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cx_posts`
--
ALTER TABLE `cx_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `cx_termmeta`
--
ALTER TABLE `cx_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cx_terms`
--
ALTER TABLE `cx_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `cx_term_relationships`
--
ALTER TABLE `cx_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `cx_term_taxonomy`
--
ALTER TABLE `cx_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `cx_usermeta`
--
ALTER TABLE `cx_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cx_users`
--
ALTER TABLE `cx_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `cx_wysija_campaign`
--
ALTER TABLE `cx_wysija_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `cx_wysija_campaign_list`
--
ALTER TABLE `cx_wysija_campaign_list`
  ADD PRIMARY KEY (`list_id`,`campaign_id`);

--
-- Indexes for table `cx_wysija_custom_field`
--
ALTER TABLE `cx_wysija_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cx_wysija_email`
--
ALTER TABLE `cx_wysija_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `cx_wysija_email_user_stat`
--
ALTER TABLE `cx_wysija_email_user_stat`
  ADD PRIMARY KEY (`user_id`,`email_id`);

--
-- Indexes for table `cx_wysija_email_user_url`
--
ALTER TABLE `cx_wysija_email_user_url`
  ADD PRIMARY KEY (`user_id`,`email_id`,`url_id`);

--
-- Indexes for table `cx_wysija_form`
--
ALTER TABLE `cx_wysija_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `cx_wysija_list`
--
ALTER TABLE `cx_wysija_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `cx_wysija_queue`
--
ALTER TABLE `cx_wysija_queue`
  ADD PRIMARY KEY (`user_id`,`email_id`),
  ADD KEY `SENT_AT_INDEX` (`send_at`);

--
-- Indexes for table `cx_wysija_subscriber_ips`
--
ALTER TABLE `cx_wysija_subscriber_ips`
  ADD PRIMARY KEY (`created_at`,`ip`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `cx_wysija_url`
--
ALTER TABLE `cx_wysija_url`
  ADD PRIMARY KEY (`url_id`);

--
-- Indexes for table `cx_wysija_url_mail`
--
ALTER TABLE `cx_wysija_url_mail`
  ADD PRIMARY KEY (`email_id`,`url_id`);

--
-- Indexes for table `cx_wysija_user`
--
ALTER TABLE `cx_wysija_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- Indexes for table `cx_wysija_user_field`
--
ALTER TABLE `cx_wysija_user_field`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `cx_wysija_user_history`
--
ALTER TABLE `cx_wysija_user_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `cx_wysija_user_list`
--
ALTER TABLE `cx_wysija_user_list`
  ADD PRIMARY KEY (`list_id`,`user_id`);

--
-- Indexes for table `cx_yoast_seo_links`
--
ALTER TABLE `cx_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `cx_yoast_seo_meta`
--
ALTER TABLE `cx_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cx_aiowps_events`
--
ALTER TABLE `cx_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_aiowps_failed_logins`
--
ALTER TABLE `cx_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_aiowps_global_meta`
--
ALTER TABLE `cx_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_aiowps_login_activity`
--
ALTER TABLE `cx_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cx_aiowps_login_lockdown`
--
ALTER TABLE `cx_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_aiowps_permanent_block`
--
ALTER TABLE `cx_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_commentmeta`
--
ALTER TABLE `cx_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_comments`
--
ALTER TABLE `cx_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_links`
--
ALTER TABLE `cx_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_options`
--
ALTER TABLE `cx_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=742;

--
-- AUTO_INCREMENT for table `cx_postmeta`
--
ALTER TABLE `cx_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `cx_posts`
--
ALTER TABLE `cx_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `cx_termmeta`
--
ALTER TABLE `cx_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_terms`
--
ALTER TABLE `cx_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_term_taxonomy`
--
ALTER TABLE `cx_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_usermeta`
--
ALTER TABLE `cx_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cx_users`
--
ALTER TABLE `cx_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_wysija_campaign`
--
ALTER TABLE `cx_wysija_campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_wysija_custom_field`
--
ALTER TABLE `cx_wysija_custom_field`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cx_wysija_email`
--
ALTER TABLE `cx_wysija_email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cx_wysija_form`
--
ALTER TABLE `cx_wysija_form`
  MODIFY `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cx_wysija_list`
--
ALTER TABLE `cx_wysija_list`
  MODIFY `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cx_wysija_url`
--
ALTER TABLE `cx_wysija_url`
  MODIFY `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_wysija_url_mail`
--
ALTER TABLE `cx_wysija_url_mail`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_wysija_user`
--
ALTER TABLE `cx_wysija_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cx_wysija_user_field`
--
ALTER TABLE `cx_wysija_user_field`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cx_wysija_user_history`
--
ALTER TABLE `cx_wysija_user_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cx_yoast_seo_links`
--
ALTER TABLE `cx_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
